"""PreferencesManager
A module to manager custom user preferences in Nuke
"""
__version__ = "1.1.1"
__author__ = "Fynn Laue"
from . import preferencesManager
