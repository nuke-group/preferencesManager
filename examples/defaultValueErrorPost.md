Hi there.
I've noticed some weird behavior when trying to set the wrong value type on a knob and trying to catch the exception.

[B]The setup:[/B]
- Create a NoOp node called 'NoOp1' with a checkbox knob called 'test'
- Run this script in the script editor:
[CODEBLOCK]NOOP = nuke.toNode("NoOp1")
try:
    NOOP.knob("test").setDefaultValue(["something"])
except TypeError as err:
    print err
print("something")[/CODEBLOCK]The exception is not caught but the script breaks with this output:
[CODEBLOCK]# Result: something
Traceback (most recent call last):
  File "<string>", line 6, in <module>
TypeError: a float is required[/CODEBLOCK]Some interesting details about this:
- The traceback points at line 6, not line 3.
- If I replace the print statement with nuke.warning("something"), the output changes:
[CODE]# Result: TypeError: a float is required[/CODE]

It seems like Nuke doesn't throw a 'proper' python error, which is probably the reason that the try/except won't work...
After some time I found this [B]workaround[/B]:
[CODEBLOCK]NOOP = nuke.toNode("NoOp1")
NOOP.knob("test").setDefaultValue(["something"])
NOOP.knob("test").defaultValue()
print("something")[/CODEBLOCK]
So apparently when retrieving the defaultValue before doing anything else the error is not thrown.
Great - fixed, right? Kind of.
Because usually when changing lots of stuff on a knob You would probably assign it to a variable first:
Guess what - this breaks the thing again:
[CODEBLOCK]NOOP = nuke.toNode("NoOp1")
knob = NOOP.knob("test")
knob.setDefaultValue(["something"])
knob.defaultValue()
print("something")[/CODEBLOCK]However, if we change the knob reference that calls defaultValue() to point directly to the knob instead of the variable, it works again:
[CODEBLOCK]NOOP = nuke.toNode("NoOp1")
knob = NOOP.knob("test")
knob.setDefaultValue(["something"])
NOOP.knob("test").defaultValue()
print("something")[/CODEBLOCK]
[B]Conclusion (so far):[/B]
It seems like the defaultValue() call resets an internal Nuke-reference that prevents the next operation from breaking the script - though I am not sure if that is actually the case or if something else is causing the strange behavior.
I'll update this post if I find out more but plase post Your thoughts and comments in case have overlooked something or You have other good ideas.
