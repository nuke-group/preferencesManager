# preferencesManager R&D and notes

## Setting knob flags

The knobs have to be added to the panel at each Nuke gui startup. Otherwise they won't be displayed properly.
This is pretty easy to solve: See the `setup` method of the `Preferences` class inside the [subPreferencesManager.py](subPreferencesManager.py) file for an example of this.

## Setting a default value other than that knob class' default

The biggest issue of writing a nice preferences manager with persistent settings in Nuke is the fact that it doesn't store default values properly.

**Example:**

A checkbox has a default value of `1.0/True`.
When it is set to this value, the settings file does not include this value, but only the knob itself.
So when Nuke reads the preferences file the next time, it doesn't know if the 'default' is `True` or `False` and so sets it to that knob class' default (which for a checkbox is `0/False`).
To prevent this, the default value always has to be set to a specific value, that is written to the file.


## Tested workarounds:

### set nuke.WRITE_ALL flag when saving the file

**Failed**

As soon as the 'OK' button is clicked in the Settings window, Nuke overwrites the file removing all knobs set to their default.

### force write value to file by check if settings have changed and overwriting the file

**Success (kind of)**
Problem: All changes made to custom settings are immideately written to the file. If Nuke crashes there is no way to undo them.
Problem:
    All knobs are reset to their default (not the value they had when initially opening the window) when the user clicks 'cancel' in the Preferences window.
    This happens because all knobs get called by knobChanged when hitting 'cancel'.
    Maybe this can be solved by registering what the knobs' values are when opening the window.

### force write knobs to file when adding a setting

**SUCCESS** \^.^/ finally

How to:
- Settings should be built in *menu.py* when Nuke starts.
- Knob defaults should be set on the knob when adding it, not on the knob itself.
- Internally the nuke.ALWAYS_SAVE flag is set so the knob value is always written to the file
```python
import nuke
from preferencesManager import Preferences
prefs = Preferences('fynn','Awesome App')
knob = nuke.Boolean_Knob("checkbox","Checkbox")
prefs.addSetting(knob, defaultValue=True)
```

This way the knob layout is preserved, knob defaults are set up and the knobs keep their values.
Nuke takes care of saving the knobs' values to the file when changing them.
Nuke takes care of reverting knobs' to their previous value when hitting 'cancel' in the Preferences window.
