"""
I'm using this file for testing different knob types. Feel free to ignore or use for your own research.
"""
import nuke
import preferencesManager


class PreferencesTests(preferencesManager.Preferences):
    """
    Testing (almost) all nuke knobs
    """
    def __init__(self):
        companyName = "Koala"
        appName = "Leefs"
        super(PreferencesTests, self).__init__(companyName, appName)

    def setup(self):
        """
        Create all necessary knobs for this app
        """

        problemKnobs = [
            nuke.Axis_Knob("axis_knob", "Axis_Knob"), # Dosn't show up :(
            nuke.EditableEnumeration_Knob("editableenumeration_knob", "EditableEnumeration_Knob", ["simple", "advanced", "supernatural"]), # editable drop-down menu. new items added to list, BUT NOT SAVED!
            nuke.FreeType_Knob("freetype_knob", "FreeType_Knob"), # Another font drop-down menu. Wors better than the other one, but doesn't have style option. Trouble saving.
            nuke.Histogram_Knob("histogram_knob", "Histogram_Knob"), # Shows node-output histogram. Useless in settings.
            nuke.Transform2d_Knob("transform2d_knob", "Transform2d_Knob"), # "argument 3 must be Matrix4, not list"
            # nuke.GeoSelect_Knob("geoselect_knob", "GeoSelect_Knob"), # RuntimeError: not allowed :(
            # nuke.Obsolete_Knob("obsolete_knob", "Obsolete_Knob"),
        ]

        testKnobs = [
            nuke.AColor_Knob("bsKnob", "BS knob"), # arrays start at 1 !!! :OOOOOOOO
            nuke.AColor_Knob("acolor_knob", "AColor_Knob"), # standard rgba color
            nuke.Array_Knob("array_knob", "Array_Knob"), # float number box ?
            nuke.BBox_Knob("bbox_knob", "BBox_Knob"), # standard x,y,r,t bbox
            nuke.Bitmask_Knob("bitmask_knob", "Bitmask_Knob", ["r", "g", "b", "a"]), # multiple checkboxes for shuffle-node style channel selection. rgb gets auto-colored
            nuke.Boolean_Knob("boolean_knob", "Boolean_Knob"), # simple checkbox
            nuke.Box3_Knob("box3_knob", "Box3_Knob"), # x,y,n,r,t,f bbox
            nuke.CascadingEnumeration_Knob("cascadingenumeration_knob", "CascadingEnumeration_Knob", ["r", "g", "b"]), # drop-down menu
            nuke.ChannelMask_Knob("channelmask_knob", "ChannelMask_Knob"), # channel selection drop-down menu
            nuke.Channel_Knob("channel_knob", "Channel_Knob"), # channel+mask selection drop-down menu
            nuke.ColorChip_Knob("colorchip_knob", "ColorChip_Knob"), # select color. Like in the 'Appearance' tab
            nuke.Color_Knob("color_knob", "Color_Knob"), # standard rgb color
            nuke.Disable_Knob("disable_knob", "Disable_Knob"), # another checkbox ?
            nuke.Double_Knob("double_knob", "Double_Knob"), # float slider
            nuke.BeginTabGroup_Knob("begintabgroup_knob", "BeginTabGroup_Knob"), # start group
            nuke.EndTabGroup_Knob("endtabgroup_knob", "EndTabGroup_Knob"), # end group
            nuke.Enumeration_Knob("enumeration_knob", "Enumeration_Knob", ["a", "b", "c"]), # drop-down menu
            nuke.EvalString_Knob("evalstring_knob", "EvalString_Knob"), # Entered values are evaluated. useful for getting environment variable values?
            nuke.Eyedropper_Knob("eyedropper_knob", "Eyedropper_Knob"), # eyedropper without channels
            nuke.File_Knob("file_knob", "File_Knob"), # Select a file/folder path
            nuke.Font_Knob("font_knob", "Font_Knob"), # font drop-down menu and bold/italic (buggy on windows)
            nuke.Format_Knob("format_knob", "Format_Knob"), # Format drop-down menu (like in nuke.root node)
            nuke.FreeType_Knob("freetype_knob", "FreeType_Knob"), # Another font drop-down menu. Wors better than the other one, but doesn't have style option. Trouble saving.
            nuke.Help_Knob("help_knob", "Help_Knob"), # the little ? in the upper right corner of every node prefs. panel. XXX useless in settings window.
            nuke.IArray_Knob("iarray_knob", "IArray_Knob"), # another float number box ?
            nuke.Int_Knob("int_knob", "Int_Knob"), # integer knob
            nuke.Keyer_Knob("keyer_knob", "Keyer_Knob"), # Key values in selected channel. XXX useless in settings
            nuke.Link_Knob("link_knob", "Link_Knob"), # Link to another knob, refer to knob name
            nuke.LookupCurves_Knob("lookupcurves_knob", "LookupCurves_Knob"), # Lookup curve. XXX Hard to configure channels
            nuke.MultiView_Knob("multiview_knob", "MultiView_Knob"), # Select views
            nuke.Multiline_Eval_String_Knob("multiline_eval_string_knob", "Multiline_Eval_String_Knob"), # Evaluate multiple lines (see EvalString_Knob)
            nuke.OneView_Knob("oneview_knob", "OneView_Knob", ["a", "b", "c"]), # Another drop-down menu
            nuke.Password_Knob("password_knob", "Password_Knob"), # entered string appears as dots. XXX Value not saved to file.
            nuke.Pulldown_Knob("pulldown_knob", "Pulldown_Knob", {"a": "eval('3*2')", "b":"definitely", "c":"noooope"}), # Advanced drop-down menu. Takes a dict. XXX what should the values point to?
            nuke.PyCustom_Knob("pycustom_knob", "PyCustom_Knob", "nuke.error('something')"), # Run custom python one-liner the first time window is opened/node is created (I think)
            nuke.PyScript_Knob("pyscript_knob", "PyScript_Knob", "print('something')"), # Run custom python script. Insert as string.
            # nuke.PyScript_Knob(name, label=None, command=None) A button that executes a Python script.(".", "."), # See previous PyScript_Knob
            nuke.Radio_Knob("radio_knob", "Radio_Knob", ["a", "b", "c"]), # Radioknobs
            nuke.Range_Knob("range_knob", "Range_Knob", [0, 0.5, 1]), # select values (between 0-1)
            nuke.Scale_Knob("scale_knob", "Scale_Knob"), # 3-dimensions (scale)
            nuke.SceneView_Knob("sceneview_knob", "SceneView_Knob", ["scene01"]), # list-style display of selectable items
            nuke.Script_Knob("script_knob", "Script_Knob", "value root.name"), # TCL script as a string
            nuke.String_Knob("string_knob", "String_Knob", "something"), # Regular good ol' string
            nuke.Tab_Knob("tab_knob", "Tab_Knob"), # Tab. XXX Settings window doesn't allow sub-tabs. New tabs are added beneath 'others' tree drop-down list
            nuke.Text_Knob("text_knob", "Text_Knob"), # text. Good for headings. If no text is entered a line is shown after label. Leave label empty for divider.
            nuke.Transform2d_Knob("transform2d_knob", "Transform2d_Knob"),
            nuke.UV_Knob("uv_knob", "UV_Knob"), # 2 dimensionsal floats
            nuke.Unsigned_Knob("unsigned_knob", "Unsigned_Knob"), # another integer knob?
            nuke.ViewView_Knob("viewview_knob", "ViewView_Knob"), # Add/remove views
            nuke.WH_Knob("wh_knob", "WH_Knob"), # 2 dimensional float slider
            nuke.XYZ_Knob("xyz_knob", "XYZ_Knob"), # 3 dimensional floats
            nuke.XY_Knob("xy_knob", "XY_Knob") # 2 dimensional floats
        ]

        # Remove all previously added knobs
        for knob in testKnobs:
            knob.setFlag(nuke.STARTLINE)
            self.removeSetting(knob.name())
        self.removeSetting("geoselect_knob")
        # Select a knob to test
        # knob = nuke.Obsolete_Knob("obsolete_knob", "Obsolete_Knob")#testKnobs[0]
        # self.addSetting(knob)
