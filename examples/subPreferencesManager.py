"""
| file: subPreferencesManger.py
| info: Example on how to create Preferences for a tool/app for Nuke
"""
import nuke
import preferencesManager


# ----------------------------------------
# CLASSES
# ----------------------------------------
class Preferences(preferencesManager.Preferences):
    """
    Example on how to create a custom Preferences class for an app based on preferencesManager.Preferences
    """
    def __init__(self):
        creatorName = "You"
        appName = "Your Tool"
        super(Preferences, self).__init__(creatorName, appName)

    def tearDown(self):
        self.removeApp(self.appName)
        self.removeCreator(self.creatorName)

    def setup(self):
        """Create settings for this app just like You would be adding knobs to a node
        """
        # Remove Settings
        # Do this when:
        #   - You've deprecated a setting in a newer version of your app
        #   - You want to change the name (not the label) of a knob
        #   - You want reset the setting's value at every restart of Nuke
        self.removeKnob("text_knob")
        self.removeKnob("assetnode_color")
        # Remove App
        # Do this when:
        #   - You have renamed your app in a newer version and want to remove the old one
        self.removeApp('My Tool')
        # Remove Creator
        # Do this when:
        #   - You have changed the creatorName and want to remove the old one
        self.removeCreator('Me')

        # Add settings
        # Path
        knob = nuke.String_Knob("plugin_location", "Plugin Location")
        knob.setValue("/it/is/right/here")
        knob.setTooltip("Set to path to the %s folder" % self.appName)
        self.addKnob(knob)
        # Icons path
        knob = nuke.EvalString_Knob("icons_location", "Icons Location")
        parentKnob = self.getKnob("plugin_location")
        knob.setEnabled(False)
        knob.setValue("[value knob.%s]/icons"%(parentKnob.name()))
        addedKnob = self.addKnob(knob)
        addedKnob.setTooltip("Set automatically.")
        # Divider
        knob = nuke.Text_Knob("divider1", "")
        self.addKnob(knob)
        # Default Task
        knob = nuke.Enumeration_Knob("enumeration_knob", "Default Task", ["comp", "roto", "tracking"])
        knob.setFlag(nuke.STARTLINE)
        self.addKnob(knob, defaultValue="comp")
        # Format
        knob = nuke.Format_Knob("format_knob", "Default Format")  # Format drop-down menu (like in nuke.root node)
        defaultFormat = '200 200 2 fun_sized_square'
        newFormat = nuke.addFormat(defaultFormat)
        knob.setValue(newFormat.name())
        self.addKnob(knob, defaultValue=newFormat.name())
        # Checkbox
        knob = nuke.Boolean_Knob("autoLoadAssets", "Auto load assets")
        knob.setValue(True)
        knob.setFlag(nuke.STARTLINE)
        self.addKnob(knob)
        # Color Chip
        knob = nuke.ColorChip_Knob("asset_node_color", "Asset Node Color")
        defaultColorInt = int('%02x%02x%02x%02x' % (int(0.0*255), int(0.8*255), int(0.3*255), 1), 16)  # float to base 16 iteger conversion
        knob.setValue(defaultColorInt)
        self.addKnob(knob, defaultValue=defaultColorInt)
        # Python button
        knob = nuke.PyScript_Knob("do_python_stuff", "Go!", "nuke.message('Done.')")
        knob.setFlag(nuke.STARTLINE)
        self.addKnob(knob)


class Preferences2(preferencesManager.Preferences):
    def __init__(self):
        creator = "You"
        appName = "Other Tool"
        super(Preferences2, self).__init__(creator, appName)
    
    def setup(self):
        knob = nuke.Boolean_Knob("something", "something")
        knob.setFlag(nuke.STARTLINE)
        self.addKnob(knob, defaultValue=False)


# ----------------------------------------
# EXEC
# ----------------------------------------
if __name__ == '__main__':
    PREFS = Preferences()
    PREFS.setup()
    PREFS2 = Preferences2()
    PREFS2.setup()
