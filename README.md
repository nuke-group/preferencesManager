<!-- TOC -->

- [Introduction](#introduction)
- [Installation](#installation)
- [Example](#example)
- [Setup](#setup)
- [Adding knobs](#adding-knobs)
- [Removing knobs](#removing-knobs)
- [Removing apps](#removing-apps)
- [Removing creators](#removing-creators)
- [Getting knob values](#getting-knob-values)
- [Setting knob values](#setting-knob-values)
- [Resetting knob values](#resetting-knob-values)
- [Reacting to changed knobs](#reacting-to-changed-knobs)
- [Notes](#notes)

<!-- /TOC -->

## Introduction

This project aims to make it easier for developers, who write plugins/tools etc. for Nuke, to create persistent preferences and show them in the Nuke preferences Window.

![](preferencesManager.png)

Below is a quick guide on how to get started.  
For a more advanced example of custom preferences look at the [subPreferencesManager.py](examples/subPreferencesManager.py) file.  


## Installation

Copy [*preferencesManager.py*](preferencesManager.py) to a location in your `sys.path` or `nuke.pluginPath()` *(for example `~.nuke/`)* or just next to your current python file.

## Example

A standard Preferences setup. See the next sections for a detailed breakdown:

```python
import preferencesManager
class MyPrefs(preferencesManager.Preferences):
    def __init__(self):
        creatorName = "You!"
        appName = "Awesome Tool"
        super(MyPrefs, self).__init__(creatorName, appName)

    def setup(self):
        # Remove old app
        self.removeApp('Old App')
        # Remove old creator
        self.removeCreator('Old Me')
        # Remove old knobs
        self.removeKnob("checkbox")
        # Add knobs
        knob = nuke.Boolean_Knob("autoLoad", "Autoload")
        knob.setFlag(nuke.STARTLINE)
        self.addKnob(knob, defaultValue=True)
        knob = nuke.String_Knob("defaultName", "Default Name")
        knob.setValue("Knob McKnobface")
        knob.setFlag(nuke.STARTLINE)
        self.addKnob(knob)

# Instantiate Preferences
PREFS = MyPrefs()

# Call function when settings change
def test(knob):
    print(knob.name(), knob.value())
PREFS.changed.connect(test)

# Set up knobs
PREFS.setup()
# Get/set values
PREFS.getKnob('autoLoad').value()
PREFS.getKnob('autoLoad').getValue()
PREFS.getKnob('autoLoad').setValue(True)
# Reset values
PREFS.resetKnob('autoLoad')
PREFS.resetAllKnobs()
```

## Setup

The best way to manage preferences for Your app is to subclass the *Preferences* class from the [*preferencesManager.py*](preferencesManager.py) module:

```python
import preferencesManager
class MyPrefs(preferencesManager.Preferences):
    def __init__(self):
        creatorName = "You!"
        appName = "Awesome Tool"
        super(MyPrefs, self).__init__(creatorName, appName)
PREFS = MyPrefs()
```

You should import your custom preferences class and run the setup when Nuke starts so all knob values are set up correctly. For example in the *menu.py* write:

```python
import myPrefsModule
myPrefsModule.MyPrefs().setup()
```

## Adding knobs

To make sure Your preferences are present when Nuke starts, add a setup method to your subclass that adds all needed knobs. Then call that method in `menu.py`:

- Create a `nuke.Knob` and pass it as the first argument to `self.addKnob`
- Set nuke flags like `nuke.STARTLINE` etc.
- Pass the knob's default in the `defaultValue` argument of the `self.addKnob` method

```python
    # (...)
    def setup(self):
        # Add knobs
        knob = nuke.Boolean_Knob("checkbox", "Autoload")
        knob.setFlag(nuke.STARTLINE)
        self.addKnob(knob, defaultValue=True)
```

## Removing knobs

If You for instance want to rename a knob in a newer version of Your app, you should first remove the old one by calling `self.removeKnob` with the name of the knob and then adding the knob with the new name:

```python
    # (...)
    def setup(self):
        # Remove old knobs
        self.removeKnob("checkbox")
        # Add knobs
        knob = nuke.Boolean_Knob("autoLoad", "Autoload")
        knob.setFlag(nuke.STARTLINE)
        self.addKnob(knob, defaultValue=True)
```

## Removing apps

To remove all knobs associated with an app:

```python
    # (...)
    def setup(self):
        # Remove old app
        self.removeApp('Old App')
```

## Removing creators

To remove all knobs (and tabs) associated with a creator:

```python
    # (...)
    def setup(self):
        # Remove old creator
        self.removeCreator('Old Me')
```

## Getting knob values

Similar to retrieving knob values on nodes: Get the knob from the Preferences then get the value from the knob:

```python
PREFS = MyPrefs()
PREFS.setup()
PREFS.getKnob('autoLoad').value()
PREFS.getKnob('autoLoad').getValue()
```

## Setting knob values

Similar to setting knob values on regular nodes. You get the knob and use its function to set the value:

```python
PREFS = MyPrefs()
PREFS.setup()
PREFS.getKnob('autoLoad').setValue(False)
```

## Resetting knob values

When the user clicks 'Restore Defaults' in the Preferences window all custom knobs are set to the default that was defined when adding the knob.

Reset a knob:

```python
PREFS = MyPrefs()
PREFS.setup()
PREFS.resetKnob('autoLoad')
```

Reset all knobs:

```python
PREFS = MyPrefs()
PREFS.setup()
PREFS.resetAllKnobs()
```

## Reacting to changed knobs

Every time a knob is changed the `changed` Signal is emits the changed knob.
Be considerate about what you connect to it since the signal is sometimes emitted for all knobs regardless if they actually changed or not:

- When setting up the preferences when Nuke opens ([this is required](#Basic-setup) for everything to work correctly)
- When clicking 'cancel' in the Settings window all knobs emit the knobChanged signal, thus also triggering the `changed` Signal for all knobs
- If you have created multiple instances of the PreferencesManager, the same knob might be returned twice depending on the context

```python
PREFS = MyPrefs()
PREFS.setup()
def test(knob):
    print(knob.name(), knob.value())
PREFS.changed.connect(test)
```

## Notes

Due to a bug in Nuke12.2v6-current the last added tab (creator) doesn't show up and makes a mysterious "Curves" tab appear. Bug report: https://support.foundry.com/hc/en-us/articles/360020761839

The Preferences Manager works around this by adding an additional tab and hiding the extra "Curves" tab in the affected Nuke versions.

*Thanks to Wouter Gilsing for his contributions to the Nuke community which inspired and helped this project.*
