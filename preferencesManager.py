"""
Manage custom (persistent) preferences and show them in the Nuke Preferences window
"""
import os
import re
import inspect
from collections import OrderedDict
import sys
if sys.version_info.major >= 3:
    from typing import Union
import nuke
# Choose between PySide and PySide2 based on Nuke version
if nuke.NUKE_VERSION_MAJOR < 11:
    from PySide import QtCore, QtGui, QtGui as QtWidgets
else:
    from PySide2 import QtGui, QtCore, QtWidgets


# ----------------------------------------
# VARIABLES
# ----------------------------------------
VERSION = "1.1.1"


# ----------------------------------------
# CLASSES
# ----------------------------------------
class Preferences(QtCore.QObject):
    """Manages custom preferences and shows them in Nuke's Preferences window:
        - Add regular Nuke knobs
        - Set defaults (even for string_knobs)
        - Emits a QSignal when a setting has changed so you can keep track
    """
    saved = QtCore.Signal(object)
    """Triggered when the preferences have been saved manually by this modules

    :return: '' / any
    :rtype: str / any
    """
    changed = QtCore.Signal(nuke.Knob)
    """Triggered when a knob is changed
    NOTE: When clicking 'cancel' in the Settings window all knobs emit the knobChanged signal, thus also triggering this class' 'changed' Signal for all knobs.

    :return: Knob that was changed
    :rtype: nuke.Knob
    """

    def __init__(self, creatorName, appName):
        # type: (str, str) -> None
        super(Preferences, self).__init__()
        # VARIABLES
        self.preferencesNode = None
        self.preferencesFile = None
        self.creatorName = creatorName
        self.appName = appName
        self.knobDefaults = {}

        # EVENTS
        nuke.addKnobChanged(self._onKnobChanged)

        # INIT
        self.preferencesNode = nuke.toNode("preferences")
        self.preferencesFile = os.path.join(os.path.expanduser("~"), ".nuke", "preferences{0}.{1}.nk".format(nuke.NUKE_VERSION_MAJOR, nuke.NUKE_VERSION_MINOR))

    def _save(self):
        """Saves the preferences to the nuke preferences
        """
        # print("saving...")
        # Build the text to be written to the file
        headerText = "Preferences {\n inputs 0\n name Preferences"
        currentPreferences = self.preferencesNode.writeKnobs(nuke.WRITE_USER_KNOB_DEFS | nuke.TO_SCRIPT | nuke.WRITE_NON_DEFAULT_ONLY)
        currentPreferences = currentPreferences.replace("\n", "\n ")
        footerText = "}\n"
        preferencesString = "{0}{1}\n{2}".format(headerText, currentPreferences, footerText)
        # Write knob values to the preferences file
        with open(self.preferencesFile, 'w') as preferencesFile:
            preferencesFile.write(preferencesString)
            self.saved.emit('')

    def _isValidKnobName(self, name, creatorName=None, appName=None):
        # type: (str, str, str) -> bool
        """Checks if the name is a valid knob name

        :param name: name to check
        :type name: str
        :return: True if valid, False if invalid
        :rtype: bool
        """
        result = False
        # Override class' creatorName
        if creatorName:
            creatorName = _sanitizeString(creatorName)
        else:
            creatorName = _sanitizeString(self.creatorName)
        # Override class' appName
        if appName:
            appName = _sanitizeString(appName)
        else:
            appName = _sanitizeString(self.appName)
        checkName = re.match(r"{0}_{1}_\w+".format(creatorName, appName), name)
        if checkName:
            result = True
        return result

    def _isValidCreatorsKnob(self, knobName, creatorName):
        # type: (str, str) -> bool
        """Checks if the specified knobName belongs to the specified creatorName

        :param knobName: Knob to check
        :type knobName: nuke.Knob
        :param creatorName: Name of the creator
        :type creatorName: str
        :return: If yes: True, if no: False
        :rtype: bool
        """
        result = False
        regExp = re.compile(r"%s_[\w]+_[\w]+"%_sanitizeString(creatorName))
        tabName = self._buildTabName(creatorName)
        if regExp.match(knobName) or knobName == tabName:
            result = True
        return result

    def _buildKnobName(self, name, creatorName=None, appName=None):
        # type: (str, str, str) -> str
        """Returns knob name with naming convention: creatorName_appName_name

        :param name: Name of the knob
        :type name: str
        :param creatorName: Name of the creator, defaults to None
        :param creatorName: str, optional
        :param appName: Name of the app, defaults to None
        :param appName: str, optional
        :return: Safe knob name
        :rtype: str
        """
        knobName = None
        checkName = self._isValidKnobName(name)
        if checkName:
            knobName = name
        else:
            # Override class' creatorName
            if creatorName:
                creatorName = _sanitizeString(creatorName)
            else:
                creatorName = _sanitizeString(self.creatorName)
            # Override class' appName
            if appName:
                appName = _sanitizeString(appName)
            else:
                appName = _sanitizeString(self.appName)
            safeName = _sanitizeString(name)
            knobName = "{0}_{1}_{2}".format(creatorName, appName, safeName)
        return knobName

    def _buildTabName(self, creatorName):
        # type: (str) -> str
        """Creates a safe tab name

        :param creatorName: Name of the creator
        :type creatorName: str
        :return: Safe tabName
        :rtype: str
        """
        tabName = "{0}_tab".format(_sanitizeString(creatorName))
        return tabName

    def _buildAppHeading(self, creatorName, appName):
        # type: (str, str) -> str
        """Builds a safe app heading name

        :param creatorName: Name of the creator
        :type creatorName: str
        :param appName: Name of the app
        :type appName: str
        :return: Safe app heading name
        :rtype: str
        """
        appHeadingName = self._buildKnobName("heading", creatorName, appName)
        return appHeadingName

    def _onKnobChanged(self):
        """Emits a QSignal with the changed knob if it is part of this Preferences class
        """
        changedNode = nuke.thisNode()
        changedKnob = nuke.thisKnob()
        if changedNode == self.preferencesNode:
            knob = self.getKnob(changedKnob.name())
            if knob:
                self.changed.emit(knob)

    def getKnob(self, knobName):
        # type: (str) -> Union[nuke.Knob, None]
        """Returns knob if it exists, Returns None if not

        :param knobName: name of the knob. Can be simple or built name
        :type knobName: str
        :return: If found: Knob, if not: None
        :rtype: nuke.Knob | None
        """
        result = None
        existingKnobNames = self.preferencesNode.knobs().keys()
        knobName = self._buildKnobName(knobName)
        if knobName not in existingKnobNames:
            nuke.debug("Knob {0} does not exist in node {1}".format(knobName, self.preferencesNode.name()))
        else:
            result = self.preferencesNode.knob(knobName)
        return result

    def addKnob(self, inputKnob, defaultValue=None):
        # type: (nuke.Knob, any) -> Union[nuke.Knob, None]
        """Adds a knob to the Preferences.
        Before adding new knobs, all existing knobs are removed to preserve order in the preferences file.
        Add knobs in the order you want them to appear in the Preferences window.

        :param inputKnob: The knob to be added
        :type inputKnob: nuke.Knob
        :param defaultValue: Default value of the knob
        :type defaultValue: any (depends on knob. see nuke API docs for more info: https://learn.foundry.com/nuke/developers/111/pythonreference/nuke-module.html)
        :return: On success: The created knob, on failure: None
        :rtype: nuke.Knob | None
        """
        addedKnob = None
        # Remove existing knobs before adding new ones to preserve order and save their values to preserve them.
        # Remove all other knobs by this creator
        currentKnobs = OrderedDict()
        creatorKnobs = OrderedDict()
        for knob in self.preferencesNode.allKnobs():
            knobName = knob.name()
            if self._isValidKnobName(knobName):
                currentKnobs[knobName] = knob
                self.preferencesNode.removeKnob(knob)
            elif self._isValidCreatorsKnob(knobName, self.creatorName):
                if knobName != self._buildTabName(self.creatorName):
                    creatorKnobs[knobName] = knob
                    self.preferencesNode.removeKnob(knob)
        # TAB
        # Remove and re-add tab to prevent knobs from being added to another creator's tab
        tabKnobName = self._buildTabName(self.creatorName)
        tabKnob = self.preferencesNode.knobs().get(tabKnobName)
        if tabKnob:
            self.preferencesNode.removeKnob(tabKnob)
        tabKnob = nuke.Tab_Knob(tabKnobName, self.creatorName)
        self.preferencesNode.addKnob(tabKnob)
        # Add other creator's knobs back
        for knobName, knob in creatorKnobs.items():
            self.preferencesNode.addKnob(knob)
        # HEADING
        # Remove and re-add heading to prevent knobs from being added to another apps's section
        headingKnobName = self._buildAppHeading(self.creatorName, self.appName)
        headingKnobLabel = "<b>{0}</b>".format(_sanitizeString(self.appName))
        headingKnob = self.getKnob(headingKnobName)
        if headingKnob:
            self.preferencesNode.removeKnob(headingKnob)
        headingKnob = nuke.Text_Knob(headingKnobName, headingKnobLabel)
        self.preferencesNode.addKnob(headingKnob)
        # New Knob
        # Add all knobs, except the heading, back again
        for knobName, knob in currentKnobs.items():
            if knobName != headingKnobName:
                self.preferencesNode.addKnob(knob)
        # NOTE: beginTab and EndTab not supported because they would just be added to the side bar
        # Don't add knob if its class is in the invalidKnobClasses list
        invalidKnobClasses = [nuke.BeginTabGroup_Knob, nuke.EndTabGroup_Knob, nuke.Tab_Knob]
        if type(inputKnob) in invalidKnobClasses:
            nuke.warning("Unfortunately it is not possible to add subtabs or groups to the Nuke Preferences window. Skipping knob '%s'"%inputKnob.name())
        else:
            # Rename input knob to safe name
            knobName = self._buildKnobName(inputKnob.name())
            inputKnob.setName(knobName)
            # Remove existing knob
            knob = self.getKnob(inputKnob.name())
            if knob:
                self.preferencesNode.removeKnob(knob)
            # Add new knob
            self.preferencesNode.addKnob(inputKnob)
            addedKnob = self.preferencesNode.knob(knobName)
            # Disable animation on the knob. This also hides other items such as 'set to default' from the context menu
            addedKnob.setFlag(nuke.NO_ANIMATION)
            # Make sure knob value is always saved to the preferences
            # NOTE (to self):
            #     If this flag ever becomes deprecated; as a workaround set the value for
            #     setDefaultValue to always be a string so Nuke knows the default value
            #     but still always writes the value to file, even when default is set:
            #     defaultValue = str(defaultValue)
            addedKnob.setFlag(nuke.ALWAYS_SAVE)
            # Set default value
            if not defaultValue:
                defaultValue = addedKnob.value()
            try:
                # Cast value type
                valueType = type(addedKnob.defaultValue())
                castValue = None
                try:
                    castValue = valueType(defaultValue)
                except ValueError:
                    nuke.error("Failed to cast {}({})".format(valueType, defaultValue))
                    pass
                addedKnob.setDefaultValue([castValue])
                # NOTE: Calling defaultValue() after setting the default call is a workaround to prevent nuke from
                # throwning a TypeError if the set defaultValue is invalid
                self.preferencesNode.knob(knobName).defaultValue()
            except AttributeError as err:
                # if knob has no defaultValue
                pass
            except TypeError as err:
                nuke.error(str(err))
            except SystemError as err:
                nuke.error(str(err))
            try:
                nuke.knobDefault("{0}.{1}".format(self.preferencesNode.name(), knobName), defaultValue)
            except TypeError as err:
                pass
            # Reset knob values to their previous state
            if knobName in currentKnobs.keys():
                addedKnob.setValue(currentKnobs.get(knobName).value())
            else:
                # If the knob dind't exist before set the current value to the default
                if defaultValue:
                    addedKnob.setValue(defaultValue)
            self.knobDefaults[addedKnob.name()] = defaultValue
        # NOTE: Fix invisible tab bug
        # Prevent infinite recursion by checking if this function was called by _fixInvisibleLastTabBug
        insp = inspect.getouterframes(inspect.currentframe())[1]
        inspFunction = insp[3]
        if inspFunction != _fixInvisibleLastTabBug.__name__:
            _fixInvisibleLastTabBug()
        # Save to file
        self._save()
        return addedKnob

    def resetKnob(self, knobName, _save=True):
        # type: (str, bool) -> None
        """Resets the specified knob to its default value

        :param knobName: Name of the knob to reset
        :type knobName: str
        :param _save: Saves new value to preferences (only for internal use)
        :type _save: bool
        """
        knob = self.getKnob(knobName)
        if knob:
            knobDefault = self.knobDefaults.get(knob.name(), None)
            if knobDefault:
                knob.setValue(knobDefault)
                self._save()
            else:
                nuke.error("No default defined for knob %s"%(knobName))
        else:
            nuke.error("No knob named %s"%(knobName))

    def resetAllKnobs(self):
        """Resets all knobs to their default value.
        """
        for knobName, defaultValue in self.knobDefaults.items():
            self.resetKnob(knobName, _save=False)
        self._save()

    def removeKnob(self, knobName):
        # type: (str) -> bool
        """Removes knob with the specified name

        :param knobName: Name of the knob to remove
        :type knobName: str
        :return: If a knob was removed: True, if not: False
        :rtype: bool
        """
        result = False
        knob = self.getKnob(knobName)
        if knob:
            self.preferencesNode.removeKnob(knob)
            result = True
        else:
            pass
        return result

    def removeApp(self, appName, creatorName=None):
        # type: (str, str) -> None
        """Remove all knobs associated with the specified appName. If creatorName is not specified the creatorName set at instanciation is used.
        If no more knobs by the specified creatorName are present after removing the app, the creator's tab is removed as well.
        Use with caution!

        :param appName: Name of the app to remove
        :type appName: str
        :param creatorName: Name of the creator of the app, defaults to None
        :param creatorName: str, optional
        """
        if not creatorName:
            creatorName = self.creatorName
        appHeadingName = self._buildAppHeading(creatorName, appName)
        appHeadingKnob = self.preferencesNode.knob(appHeadingName)
        if appHeadingKnob:
            for knob in self.preferencesNode.allKnobs():
                knobName = knob.name()
                if self._isValidKnobName(knobName, creatorName, appName):
                    self.preferencesNode.removeKnob(knob)
            nuke.warning("Removed all knobs associated with %s by %s"%(appName, creatorName))
        # Remove creator's if he has no knobs except his tab left
        creatorKnobs = []
        for knob in self.preferencesNode.allKnobs():
            knobName = knob.name()
            if self._isValidCreatorsKnob(knobName, creatorName):
                creatorKnobs.append(knob)
        if len(creatorKnobs) == 1:
            self.removeCreator(creatorName)
        self._save()

    def removeCreator(self, creatorName):
        # type: (str) -> None
        """Remove all knobs associated with the specified creatorName. Use with caution!

        :param creatorName: [description]
        :type creatorName: [type]
        """
        tabName = self._buildTabName(creatorName)
        tabKnob = self.preferencesNode.knob(tabName)
        if tabKnob:
            for knob in self.preferencesNode.allKnobs():
                knobName = knob.name()
                if self._isValidCreatorsKnob(knobName, creatorName):
                    self.preferencesNode.removeKnob(knob)
            # self.preferencesNode.removeKnob(tabKnob)
            nuke.warning("Removed all knobs associated with %s"%creatorName)
            self._save()


# ----------------------------------------
# FUNCTIONS
# ----------------------------------------
def _sanitizeString(inputString):
    # type: (str) -> str
    """Remove unsafe characters from string

    :param inputString: String to sanitize
    :type inputString: str
    :return: Sanitized string
    :rtype: str
    """
    sanitizedString = inputString
    regExp = re.compile(r"[^\w]+")
    sanitizedString = regExp.sub("", inputString)
    return sanitizedString


def _fixInvisibleLastTabBug():
    """Fixes bug ID 471966 (https://support.foundry.com/hc/en-us/articles/360020761839),
        that causes last added tab to not show and a strange "Scopes" tab to show,
        by adding an extra tab and making Scopes tab invisible
    Nuke 12.2v5 - 13.1v1 adds an extra "Scopes" TAB_KNOB to the Preferences and hides the last TAB_KNOB in the Preferences
    """
    nukeVersionNumber = float("{}.{}{}".format(nuke.NUKE_VERSION_MAJOR, nuke.NUKE_VERSION_MINOR, nuke.NUKE_VERSION_RELEASE))
    creatorName = "PREFERENCES_MANAGER_EMPTY_CREATOR"
    appName = "PREFERENCES_MANAGER_EMPTY_APP"
    knobName = "PREFERENCES_MANAGER_EMPTY_KNOB"
    prefs = Preferences(creatorName, appName)
    # Remove all knobs first to ensure that:
    # - It's the last one added (if it's added)
    # - It's not shown if the bug has been fixed
    prefs.removeKnob(knobName)
    prefs.removeApp(appName)
    prefs.removeCreator(creatorName)
    # Hide additional "Scopes" TAB_KNOB
    if 12.25 <= nukeVersionNumber <= 13.11:
        # Hide additional Scopes knob
        for knob in [[k for k in prefs.preferencesNode.allKnobs() if "Scopes" in k.label()][-1]]:
            knob.setFlag(nuke.INVISIBLE)
        # Add an extra TAB_KNOB in versions where Nuke hides the last one
        knob = nuke.Text_Knob(knobName, "")
        prefs.addKnob(knob)
